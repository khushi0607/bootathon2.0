function dynamic():void {

    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("input"); 
    var table:HTMLTableElement = <HTMLTableElement>document.getElementById("table_1");

    var count:number = 1;
    var num:number = +input.value;//converting the element of id into number
    if(isNaN(num)){
        alert("Enter a no. only");
    }

    while( table.rows.length > 1)   { 
        table.deleteRow(1);
    }

    for(count=1 ; count<=num ; count++) {  //loop for  printing the table till the given number
  
        var row:HTMLTableRowElement = table.insertRow();//add table row during runtime

        var cell:HTMLTableDataCellElement = row.insertCell();//first column
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = num.toString(); 
        cell.appendChild(text);//this function add data to the new cell

       
        var cell:HTMLTableDataCellElement = row.insertCell();//second column
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();//third column
        var text:HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (num*count).toString();
        cell.appendChild(text);
    }

}