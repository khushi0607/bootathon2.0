# <u>TEXT SUMMARIZATION</u>

## <u> Theory</u>

**The  text summarizer is a platform where the user will paste or type text in form of sentences and paragraphs and the summarizer will output the summary of the story in about five or seven sentences. The text summarizer will also output a bar graph of the five most frequent mentioned words in the chunk of text. For optimal results, the text summarizer should have a large chunk of text in the textbox to create the summary and the bar graph.**


**Text summarization is a subdomain of Natural Language Processing (NLP) that deals with extracting summaries from huge chunks of texts. The text summarizer toolkit will follow the following steps in order to come up with the summary:**

>1. It takes a text as input.</style>

**Example:-**

*So, keep working. Keep striving. Never give up. Fall down seven times, get up eight. Ease is a greater threat to progress than hardship. Ease is a greater threat to progress than hardship. So, keep moving, keep growing, keep learning. See you at work*

>2. It splits the text into one or more paragraph(s).

**Example:-**

*So, keep working. Keep striving. Never give up. Fall down seven times, get up eight. Ease is a greater threat to progress than hardship. Ease is a greater threat to progress than hardship. So, keep moving, keep growing, keep learning. See you at work.*

>3. It splits each paragraph into one or more sentence(s).

**Example:-**

*So, keep working
Keep striving
Never give up
Fall down seven times, get up eight
Ease is a greater threat to progress than hardship
Ease is a greater threat to progress than hardship
So, keep moving, keep growing, keep learning
See you at work*
>4. Split each sentence(s) into one or more words.(Tokenizing the Sentences)

**Example:-**

*['keep', 'working', 'keep', 'striving', 'never', 'give',
'fall', 'seven', 'time', 'get', 'eight', 'ease',
'greater', 'threat', 'progress', 'hardship', 'ease', 'greater',
'threat', 'progress', 'hardship', 'keep', 'moving', 'keep',
'growing' ,'keep' ,'learning' , 'see', 'work']*

>5. Gives each sentence weight-age by comparing its word to predefined dictionary called “stopword.txt” or in our case the nltk stopword library. If some word of sentence matches to any word with the predefined dictionary, then the word is considered as low weighted. We can find the weighted frequency of each word by dividing its frequency by the frequency of the most occurring word.

 Word	        | Frequency	    | Weighted Frequency  |
| ------------- |:-------------:| -----:|
|  eight     | 1 | 0.2 |
| ease      | 2   | 0.4 |
| fall | 1     |    0.2 |
|  get     | 1 | 0.2 |
| give     | 1   | 0.2 |
| greater | 2    |    0.4 |
| growing    | 1 | 0.2 |
| hardship    | 2   | 0.4 |
| keep | 5  |    1.0 |

In this case the word keep is the most frequent word as it is mentioned 5 times. To find the weighted frequency, we divide the frequency of each word with 5, thus the word keep will have the weighted frequency of 1 as others follow.

>6. The next step is to plug the weighted frequency in place of the corresponding words in original sentences and finding their sum.

**Example:-**

|  Sentence      | Sum of Weighted Frequency  | 
| ------------- |:-------------:|
| So, keep working  | 1 + 0.20 = 1.20   | 
| Keep striving     | 1 + 0.20 = 1.20    |  
| Never give up |0.20 + 0.20 = 0.40      |
| Fall down seven times, get up eight  |0.20 + 0.20 + 0.20 + 0.20 + 0.20 = 1.0| 
| Ease is a greater threat to progress than hardship     | 0.40 + 0.40 + 0.40 + 0.40 + 0.40 = 2.0  |  
| So, keep moving, keep growing, keep learning |1 + 0.20 + 1 + 0.20 + 1 + 0.20 = 3.60|

>7. The final step is to sort the sentences in inverse order of their sum. The sentences with highest frequencies summarize the text. For instance, look at the sentence with the highest sum of weighted frequencies:

**Example:-**

*So, keep moving, keep growing, keep learning*

*You can easily judge that what the paragraph is all about. Similarly, you can add the sentence with the second highest sum of weighted frequencies to have a more informative summary. Take a look at the following sentences: So, keep moving, keep growing, keep learning. Ease is a greater threat to progress than hardship.*


#### For more Experiments go to <http://vlabs.iitb.ac.in/>